Somatropin
Somatrem
Sermorelin
Tesamorelin
Mecasermin
Octreotide
Pegvisomant
Bromocriptine
Cabergoline
Protirelin
Thyrotropin
Cosyntropin
Gonadorelin
Goserelin
Histrelin
Leuprolide
Nafarelin
Ganirelix
Cetrorelix
Follitropin
Urofollitropin
Conivaptan
Tolvaptan
Levothyroxine
Liothyronine
Perchlorate
Thiocyanate
Pertechnetate
Tc-99m pertechnetate
I-131
Iodide (high concentrations)
Propylthiouracil
PTU
Methimazole
beta-Blockers as inhibitor of peripheral thyroid metabolism
Ipodate
Prednisone
Prednisolone
Methyprednisolone
Dexamethasone
Hydrocortisone
Fluticasone
Beclomethasone
Flunisolide
Triamcinolone
Budesonide
Mifepristone as glucocorticoid receptor antagonist
RU-486 as glucocorticoid receptor antagonist
Mitotane
Aminoglutethimide
Metyrapone
Trilostane
Ketoconazole as inhibitor of glucocorticoid synthesis
Fludrocortisone
Spironolactone
Eplerenone
Dehydroepiandrosterone
Finasteride
Dustasteride
Anastrozole
Letrozole
Exemestane
Formestane
Tamoxifen
Clomiphene
Bazedoxifene
Ospemifene
Raloxifene
Fulvestrant
Flutamide
Spironolactone as androgen receptor antagonist
Mifeprisone
RU-486
Ehinyl estradiol
Mestranol
Norgestrel
Levonorgestrel
Norethindrone
Norethindrone acetate
Ethynodiol
Norgestimate
Gestodene
Desogestrel
Drospirenone
Medroxyprogesterone acetate (injectable)
Etonogestrel (silastic implant)
Testosterone enanthate
Testosterone cypionate
Estrogen + progestin
Alendronate
Risedronate
Ibandronate
Pamidronate
Zoledronate
Donosumab
Salmon Calcitonin
hPTH 1-34
Teriparatide
hPTH 1-84
Fluoride as bone anabolic agents
Aluminum hydroxide as oral phosphate binder
Calcium carbonate as oral phosphate binder
Sevelamer
Cholecalciferol
Ergocalciferol
Calcifediol
Calcitriol
Doxercalciferol
Paricalcitol
Cinacalcet
Calcium gluconate
Calcium carbonate
Calcium citrate-malate
Potassium phosphate
Acarbose
Miglitol
Voglibose
Regular insulin
Insulin lispro
Insulin aspart
Insulin glulisine
NPH insulin
Insulin glargine
Insulin detemir
Acetohexamide
Chlorpropamide
Tolazemide
Tolbutamide
Glimepiride
Glipizide
Glibenclamide
Glyburide
Gliclazide
Gliquidone
Metformin
Pramlintide
Exenatide
Liraglutide
Sitagliptin
Saxagliptin
Piloglitazone
Rosiglitazone
Diazoxide
Glucagon