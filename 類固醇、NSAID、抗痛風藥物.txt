Aspirin
Ketoprofen
Traditional NSAID
COX-2 inhibitor
Fenoprofen
Ibuprofen
Tolmetin
Naproxen
Indomethacin
Flurbiprofen
Ketorolac
Lumiracoxib
Etoricoxib
Rofecoxib
Valdecoxib
Meloxicam
Celecoxib
Diclofenac
Sulindac
Piroxicam
Oxaprozin
Nabumetone
Interaction: NSAID + ACEI = ?
Interaction: NSAID + Corticosteroids = ?
Interaction: NSAID + Aminoglycosides = ?
Interaction: Ibuprofen + aspirin = ?
Salicyclism = ?
Acetaminophen
Panadol(R)
Scanol(R)
N-acetylcysteine
Allopurinolo
Febuxostat
Colchicine
NSAIDs
Corticosteroids
Probenacid
high-dose salycylates
Diuretics
low-dose salicylates
Pegloticase
Uricosurics
Uricase-like
Sulfinpyrazone
Benzbromarone
Rasburicase
Glucocorticoid
Hydrocortisone
Prednisolone
Methylprednisolone
Dexamethasone
Hydroxychloroquine anti-inflammatory activity
DMARDs
Muromonab
OKT3
Visilizumab
Basiliximab
Adalimumab
Certolizumab
Etanercept
Golimumab
Infliximab
Secukinumab
Ixekizumab
Brodalimumab
Anakinra
Canakinumab
Ustekinumab
Guselkumab
Omalizumab
