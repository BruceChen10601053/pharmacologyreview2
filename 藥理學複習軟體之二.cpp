/* 藥理學複習軟體之二程式碼
陳光穎 Bruce Chen
2021/8/14
版本: 1.0.0.0
限制: 每個檔案最多1000個藥名(700以上顯示警告)，每個藥名最長1000位元組(700以上顯示警告)，類別名最長19位元組(不顯示警告)，新增檔案或修改檔名必須修改程式碼(不顯示警告)，檔名最長99位元組(不顯示警告) */

/* 增加檔案(類別)時，請修改
1. 第22~27行
2. 第16行 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define NUM_FILES 21 // number of files (categories)

/* Global variable */
char WordBank[NUM_FILES][1000][1001]; // 藥名 word bank
int N[NUM_FILES]; // number of drugs in each file (category)
int Weights[NUM_FILES]; // weights of each file (category)
char Categories[NUM_FILES][20] = {"內分泌藥理", "自主神經藥理", "自主神經藥理2", "抗癲癇藥", "抗癲癇藥2", "物質濫用", "神內精神麻醉", "細菌黴菌病毒原蟲", "鴉片", "抗腫瘤藥", "降血脂凝血貧血", "降血脂凝血貧血2", "降血壓藥利尿劑", "Some newer drugs", "PK, PD", "PG, 5-HT, 組織胺", "抗發炎藥物", "免疫抑制劑", "胃腸道用藥", "呼吸道用藥", "毒理學"};
// name of categories
char CategoriesW[NUM_FILES][20] = {"Meta", "ANS", "ANS2", "ASD", "ASD2", "Abuse", "CNS", "Antibiotics", "Opiate", "Cancer", "CV1", "CV1-2", "CV2+Nephro", "Some newer drugs", "PK, PD", "Autocrine", "Anti-inflammatory", "Immunosuppressant", "GI", "CM", "Other"};
// name of categories in 權重.txt
char Files[NUM_FILES][100] = {"內分泌藥理.txt", "自主神經藥理.txt", "自主神經藥理2.txt", "抗癲癇藥.txt", "抗癲癇藥2.txt", "物質濫用.txt", "神內精神麻醉藥物_newline.txt", "細菌黴菌病毒原蟲.txt", "鴉片.txt", "抗腫瘤藥.txt", "降血脂凝血貧血.txt", "降血脂凝血貧血2.txt", "降血壓藥利尿劑.txt", "Some_newer_drugs.txt", "PK, PD.txt", "PG, 5-HT, 組織胺.txt", "類固醇、NSAID、抗痛風藥物.txt", "免疫抑制劑.txt", "胃腸道用藥.txt", "呼吸道用藥.txt", "毒理學.txt"};
// name of files

/* User-defined functions */
int Ran2Cat(double x){ // 輸入0~1之間的隨機數字，依據權重輸出抽到的類別index

    int N_total = 0; // 分母
    double CDF[NUM_FILES+1] = {0}; // CDF[i+1] = 從weights[0]累加到weights[i]
    int i;
    for(i=0; i<NUM_FILES; i++){
        N_total += Weights[i];
        CDF[i+1] = CDF[i] + Weights[i];
    }
    for(i=0; i<=NUM_FILES; i++){
        CDF[i] /= N_total;
    }

    for(i=0; i<NUM_FILES; i++){
        if(x < CDF[i+1]){
            break;
        }
    }

    return i;
}

/* 程式進入點 */
int main(){
     /* Opening files */
    printf("藥理學複習軟體\r\n本程式隨機輸出一個藥物名稱，請你說出他的適應症和作用機轉。\r\n");
    FILE* A[NUM_FILES] = {NULL, };
    int j;
    for(j=0; j<NUM_FILES; j++){
        A[j] = fopen(Files[j], "r");
        if(A[j] == NULL){
            printf("錯誤: File not found!: %s\n請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n", Files[j]);
        }
    }
    FILE* B = fopen("權重.txt", "r");
    if(B == NULL){
        printf("錯誤: File not found!: 權重.txt\n請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n");
    }

    /* Loading information from files */
    for(j=0; j<NUM_FILES; j++){
        char s[40];
        strcpy(s, CategoriesW[j]);
        strcat(s, ": %d\n");
        int _ = fscanf(B, s, &Weights[j]);
        if(_ != 1){
            printf("錯誤: 權重檔案格式錯誤: Line %d (%s)\n請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n", j+1, CategoriesW[j]);
        }
    }
    int N_ = 0;
    for(j=0; j<NUM_FILES; j++){
        N_ += Weights[j];
    }
    for(j=0; j<NUM_FILES; j++){
        printf("%s (%s) 權重: %d / %d\n", Categories[j], CategoriesW[j], Weights[j], N_);
    }

    for(j=0; j<NUM_FILES; j++){
        int i;
        for(i=0; i<1000; i++){
            char* f = fgets(WordBank[j][i], 1000, A[j]);

            if(f == NULL){
                break;
            }
            if(strlen(WordBank[j][i]) > 700){
                printf("警告: 有藥物名稱過長(%s Line %d)。請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n", Files[j], i+1);
            }
        }

        N[j] = i;
        printf("%s資料庫有%d個\r\n", Categories[j], N[j]);
        if(N[j] >= 700){
            printf("警告: 資料庫藥物過多(%s)。請聯絡開發者!\r\n開發者: 陳光穎 Bruce Chen\r\n", Files[j]);
        }
    }

    /* Initialization */
    srand(time(NULL)); // 設定亂數種子
    printf("請按{Enter}繼續 . . . ");
    char __[20];
    gets(__);

    /* 考你 */
    while(1){
        double r = (double)rand() / RAND_MAX; // 產生 0~1 的亂數
//printf("%lf\n", r);
        int R = Ran2Cat(r);
//printf("%d\n", R);
        if(N[R] == 0){
            continue;
        }
        int rr = rand() % N[R]; // 產生 0~N[R] 的亂數

        printf(WordBank[R][rr]);
        printf("請按{Enter}繼續 . . . ");
        gets(__);
    }
}

