# 藥理學複習軟體

距離考試還有1天，我們來玩個遊戲吧! <br>
{聽說考前1天投資報酬率最高的事是看藥名(?)}

## 介紹

本程式隨機輸出一個藥物名稱，請你說出他的適應症和作用機轉。(心裡默念就好，不須打字)

## 特色

每個藥物出現的機率依據國考出題頻率加權 (如內分泌 2/25, 化療(抗生素+抗腫瘤) 4.5/25)

## 使用說明

下載 "藥理學複習軟體之二.zip"。解壓縮後，執行 "藥理學複習軟體之二.exe"。<br>
按{Enter}輸出藥物名。作答完畢請再按{Enter}進入下一題。
下載點: https://gitlab.com/BruceChen10601053/pharmacologyreview2/-/releases

## 相關作品
[肝達人](https://gitlab.com/BruceChen10601053/EasyLiver/-/releases)

[胃達人](https://gitlab.com/BruceChen10601053/easystomach/-/releases)

[細菌達人](https://gitlab.com/BruceChen10601053/easybacteria/-/releases)

